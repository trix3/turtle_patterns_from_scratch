module GlossTurtles where
 
import Prelude hiding (Left, Right)
import           Control.Concurrent
import           Control.Concurrent.MVar
import           Graphics.Gloss
import           Graphics.Gloss.Data.ViewPort
import           Graphics.Gloss.Interface.IO.Animate
import           Graphics.Gloss.Interface.IO.Simulate
import qualified Sound.Tidal.Context                 as T
import           ParseTurtles
import           IntermediateNotation
import           TidalEvents

import Data.Time
import Data.Time.Clock.POSIX
import qualified Data.List as L

windowWidth, windowHeight :: Int
windowWidth = 800
windowHeight = 600

displayWindow :: Display
displayWindow = (InWindow "Hello, World" (windowWidth, windowHeight) (0, 0))

simulationRate :: Int
simulationRate = 2

zero :: Float
zero = 0

type Model = [(Float, Float, Float)]

initialModel :: Model
initialModel = [(0, 0, 90)]

apattern =  T.cat $ map pure [
                  PosTurtle { pos = (0, 0), move = Forward 10 }
                , PosTurtle { pos = (10, 0), move = Right 90  }
            ]

--mandala' = T.fastcat $ map pure [Forward 10, Right 45, Forward 20, Left 90, Forward 10]
mandala'' = T.slow "1 1 2 3 5 8" $ T.cat $ map pure [Forward 10, Right 45, Forward 20, Left 90, Forward 10]
--mandala = T.cat $ map pure [Forward 10, Right (pi/4), Forward 10, Left (pi/4)]
square = T.cat $ map pure [Forward 1, Right (90)]
triangle = T.cat $ map pure [Forward 1, Right (120), Forward 1, Right (120), Forward 1, Right (120)]
chaosmap = T.slow "1 1 2 3 5 8" ("f l l" :: T.Pattern Turtle)

screenThread :: IO (MVar (T.Pattern Turtle))
screenThread = do
    mvPattern <- newMVar $ chaosmap 
    posixTime <- getPOSIXTime
    --print $ posixTime
    threadId <- forkIO $ simulatePattern mvPattern posixTime
    return mvPattern

--simulatePattern :: (MVar (T.Pattern Turtle)) ->  IO ()
simulatePattern mvPattern startTime =
    simulateIO
        displayWindow
        white
        simulationRate
        initialModel
        modelToPicture
        (updatePicture mvPattern startTime)

modelToPicture :: Model -> IO Picture
modelToPicture model = do
    return $ pictures $ _model2Pictures model

_model2Pictures :: [(Float, Float, c)] -> [Picture]
_model2Pictures [] = []
_model2Pictures ((x,y,_):[]) = [] 
_model2Pictures ((x,y,_a):(x',y',_b):xs) = (_model2Picture' (x,y) (x',y'): _model2Pictures xs')
     where xs' = ((x',y',_b):xs)

_model2Picture' :: (Float, Float) -> (Float, Float) -> Picture
_model2Picture' (x,y) (x',y') = Line [(10 * x, 10 * y), (10 * x',10 * y')]

timeSinceStart :: POSIXTime  -> POSIXTime -> Rational 
timeSinceStart currentTime startTime = (toRational currentTime) - (toRational startTime)

--updatePicture :: MVar(T.Pattern Turtle) -> ViewPort -> Float -> Model -> IO Model
updatePicture mvPat startTime _ intervalTime model = do
    pat <- readMVar mvPat
    currentTime <- getPOSIXTime 
    let turtleValues = multiplyEventsMap $ patternValues pat $ timeSinceStart currentTime startTime
    --print $ "Time: " ++ show (fromRational (timeSinceStart currentTime startTime))
    --
    print $ "Values: " ++ show turtleValues
    let (x, y, theta) = last model
    --print $ "last x y and theta" ++ show (x, y, theta)
    let model' = L.scanl (turtle2Model) (x, y, theta) turtleValues
    --print $ "model': " ++ show (model ++ model')
    return $ model ++ model'
    {-
    return model
    return $ [(0, 0, 90), (0, 1, 0), (1, 1, 90), (2, 1, 0)] 
    if () 
       then 
           return model'
       else 
           return model
    -}
--turtle2Model :: Model -> Turtle -> Model

multiplyByTime :: T.Time -> Turtle -> Turtle
multiplyByTime time turtle = multiplyT (fromRational time) turtle

multiplyEventsMap :: [(T.Time, Turtle)] -> [Turtle]
multiplyEventsMap = map (\(f, t) -> multiplyByTime f t)

multiplyT' :: Float -> Float -> Turtle -> Turtle
multiplyT' lengthFactor angleFactor turtle = case turtle of 
       Forward a -> Forward (a * lengthFactor)
       Right   a -> Right   (a * angleFactor)
       Left    a -> Left    (a * angleFactor)

multiplyT :: Float -> Turtle -> Turtle
multiplyT factor turtle = case turtle of
       Forward a -> Forward (a * factor)
       Right   a -> Right   (a * factor)
       Left    a -> Left    (a * factor)

multiplyTByFactors :: Float -> Float -> [Turtle] -> [Turtle]
multiplyTByFactors lengthFactor angleFactor turtles = 
       map (\t -> multiplyT' lengthFactor angleFactor t) turtles

turtle2Model :: (Float, Float, Float) -> Turtle -> (Float, Float, Float)
turtle2Model (x, y, theta) turtle = (x', y', (radian2Degrees theta'))
        where theta' = deg2Radians (heading turtle theta)
              (x', y') = coordinates (x, y) (turtleDistance turtle) theta'

coordinates :: (Float, Float) -> Float -> Float -> (Float, Float)
coordinates (x, y) distance theta = (x + x', y + y')
    where (x', y') = changeInCoordinates (moveByUnits distance simulationRate) theta

moveByUnits :: Float -> Int -> Float
moveByUnits distance simulationRate = distance / (fromIntegral simulationRate)

changeInCoordinates :: Float -> Float -> (Float, Float)
changeInCoordinates moveUnits theta = (x*moveUnits, y*moveUnits)
    where (x, y) = roundedCoordinates theta

roundedCoordinates :: Float -> (Float, Float)
roundedCoordinates theta = (roundTo 3 x, roundTo 3 y)
    where (x, y) = (polarCoordinates theta)

polarCoordinates :: Float -> (Float, Float)
polarCoordinates theta = (cos theta , sin theta)

deg2Radians :: Float -> Float
deg2Radians a = (pi / 180) * a

radian2Degrees :: Float -> Float
radian2Degrees a = (180 / pi) * a

roundTo :: Int -> Float -> Float
roundTo n x = (fromInteger $ round $ x * (10^n)) / (10.0^^n)

heading :: Turtle -> Float -> Float
heading turtle theta = case turtle of 
            Right a    -> theta + a
            Left a     -> theta - a
            Forward a  -> theta 

turtleDistance :: Turtle -> Float
turtleDistance turtle = case turtle of 
            Right a    -> zero
            Left a     -> zero
            Forward a  -> a 

--patternValues :: T.Pattern Turtle -> Float -> [Turtle]
patternValues pat t = zip (eventTimes evs) $ multiplyTByFactors (0.5) (pi/2) $ map T.value $ evs 
                          where evs = sortByPart $ arcEvents pat (T.Arc t' (t' + 1)) 
                                t' = toRational $ floor t
---
myAnimate mvPattern = 
    animateIO 
         displayWindow
         white
        (picture mvPattern)
         controllerSetRedraw

timeArc :: Float -> T.ArcF Rational
timeArc t = T.Arc (toRational t) $ toRational $ t + 1 

picture :: MVar (T.Pattern Turtle) -> Float -> IO Picture
picture mvPattern t =
    do
       pat <- readMVar mvPattern
       let values = map T.value $ sortByWhole $ arcEvents
                        pat 
                        (timeArc t)
       print values
       return $ scale 20 20 $ drawLines values

drawLine :: (Float, Float) -> [Turtle] -> IO (Picture)
drawLine _ [] = do
    return mempty

drawLines :: [Turtle] -> Picture
drawLines []         = mempty
drawLines ((Left angle):ts) = rotate angle $ drawLines ts
drawLines ((Right angle):ts) = rotate (0 - angle) $ drawLines ts
drawLines ((Forward len):ts) = translate len 0 $ pictures [line [(0,0), (-len,0)], drawLines ts]

